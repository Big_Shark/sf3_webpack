sf3_webpack
===========

Manual for integration webpack to sf3

1) install sf
```
symfony new my_project_name
```

2) open directory
```
cd my_project_name
```

3) Create new npm project
```
npm init
```

4) Add webpack and webpack loaders
```
npm install webpack css-loader style-loader node-sass sass-loader assets-webpack-plugin extract-text-webpack-plugin es6-promise --save-dev                
```

5) Create webpack.config.js in root current directory
```js
require('es6-promise').polyfill();
var webpack = require('webpack')
var ExtractTextPlugin  = require('extract-text-webpack-plugin')
var AssetsPlugin  = require('assets-webpack-plugin')
var path = require('path');

module.exports = {
  entry   : {
    'app': ["./app/Resources/assets/js/app.js"]
  },
  output: {
    filename: "[name]_[hash].js",
    path : 'web/assets/',
    publicPath : '/assets/'
  },
  module: {
    loaders: [
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract(
          'style-loader',
          'css!sass')
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loaders: [
          'url?limit=10000&name=[name].[ext]'
        ]
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin('[name]_[contenthash].css'),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': "'production'"
      }
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery"
    }),
    new AssetsPlugin()
  ]
};
```

6) Add webpack-assets-bundle
```
composer require ju1ius/webpack-assets-bundle
```
```php
// app/AppKernel.php
public function registerBundles () {
  $bundles = array(
    //...
    new ju1ius\WebpackAssetsBundle\Ju1iusWebpackAssetsBundle(),
    //...
  );
}
```

7) Create dirs and files
```
mkdir app/Resources/assets/js
mkdir app/Resources/assets/scss
touch app/Resources/assets/js/app.js
touch app/Resources/assets/scss/app.scss
```

8) Include sass in js
```
echo 'require("../scss/app.scss")' > app/Resources/assets/js/app.js
```

9) Change template, use webpack_asset function
```
<link href="{{ webpack_asset('app', 'css') }}" rel="stylesheet">
```
```
<script type='text/javascript' src='{{ webpack_asset('app') }}'></script>
```

10) Add shortcut to package.json
```
  "scripts": {
    "dev": "./node_modules/webpack/bin/webpack.js -d -w",
    "build": "./node_modules/webpack/bin/webpack.js -p"
  }
```

11) Run
```
npm run dev
```
or
```
npm run build
```

12) Enjoy :)