require('es6-promise').polyfill();
var webpack = require('webpack')
var ExtractTextPlugin  = require('extract-text-webpack-plugin')
var AssetsPlugin  = require('assets-webpack-plugin')
var path = require('path');

module.exports = {
  entry   : {
    'app': ["./app/Resources/assets/js/app.js"]
  },
  output: {
    filename: "[name]_[hash].js",
    path : 'web/assets/',
    publicPath : '/assets/'
  },
  module: {
    loaders: [
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract(
          'style-loader',
          'css!sass')
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loaders: [
          'url?limit=10000&name=[name].[ext]'
        ]
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin('[name]_[contenthash].css'),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': "'production'"
      }
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery"
    }),
    new AssetsPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.optimize.OccurenceOrderPlugin(true)
  ]
};